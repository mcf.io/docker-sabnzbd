# mcf.io/docker-sabnzbd
[SABnzbd](https://sabnzbd.org) is a multi-platform binary newsgroup downloader.
The program works in the background and simplifies the downloading verifying and extracting of files from Usenet. SABnzbd uses NZB files (similar to .torrent files, but for Usenet), instead of browsing Usenet directly. NZBs can be acquired from a variety of usenet indexing services.

## Usage
```
docker create \
  --name sabnzbd \
  -p 8080:8080 \
  -e PUID=<UID> -e PGID=<GID> \
  -v /etc/localtime:/etc/localtime \
  -v </path/to/appdata>:/config \
  -v </path/to/downloads>:/downloads \
  mcfio/sabnzbd
```

## Parameters
Docker parameters are split into two components, separated by a colon, the left size represents the host and the right the container.  Example, the -p 8989:8989 parameter identifies the port mapping from host to container.

* `-p 8080:8080` - mapping host:container port for Sonarr web interface
* `-v </path/to/appdata>:/config` - Sonarr config and database directory
* `-v </path/to/downloads>:/downloads` - host volume mapping for downloads

The container is based on alpine linux, to gain access to the shell execute `docker exec -it sonarr sh`

### User / Group IDs
At times, the host volume permissions may not match those of IDs within the running container.  To resolve this the container allows specifying the `PUID` and `PGID`.  Ensure the host volume permissions match those of the container.

For example, either create or use an existing user on the host system and use the values for uid and gid.
```
$ id <docker user>
uid=1001(dockeruser) gid=1001(dockeruser) groups=1001(dockeruser)
```

## Info
You can monitor the logs for the container using `docker logs -f sonarr`.

* Container version number
  * `docker inspect -f '{{ index .Config.Labels "org.label-schema.vcs-ref" }}' sabnzbd`

* image version number
  * `docker inspect -f '{{ index .Config.Labels "org.label-schema.vcs-ref" }}' mcfio/sabnzbd`
