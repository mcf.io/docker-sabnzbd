FROM python:2.7-alpine as builder

RUN apk add --no-cache \
  automake \
  autoconf \
  build-base \
  git \
  libffi-dev \
  openssl-dev \
  jq

RUN pip install --install-option="--prefix=/build/pip" --upgrade \
  sabyenc \
  cheetah3 \
  cryptography \
  requests \
  requests[security] \
  requests-cache \
  babelfish \
  "guessit<2" \
  stevedore==1.19.1 \
  python-dateutil \
  "subliminal<2" \
  qtfaststart

WORKDIR /build
RUN git clone --depth=1 https://github.com/Parchive/par2cmdline.git &&\
  cd par2cmdline &&\
  aclocal &&\
  automake --add-missing &&\
  autoconf &&\
  ./configure --prefix=/usr &&\
  make
RUN git clone --depth=1 https://github.com/mdhiggins/sickbeard_mp4_automator.git mp4_automator

WORKDIR /build/SABnzbd
RUN set -o pipefail &&  wget $(wget -qO - https://api.github.com/repos/sabnzbd/sabnzbd/releases | jq -r "[ .[] | .assets[] | select(.name | test(\"SABnzbd-[0-9.]+-src.tar.gz\")) ] | first | .browser_download_url") -qO - | tar -xz --strip 1

FROM alpine:3.8

ARG BUILD_DATE
ARG VCS_REF

LABEL org.label-schema.schema-version="1.0" \
  org.label-schema.build-date="${BUILD_DATE}" \
  org.label-schema.name="SABnzbd - Free and easy binary newsreader" \
  org.label-schema.url="https://sabnzbd.org/" \
  org.label-schema.vcs-ref="${VCS_REF}" \
  org.label-schema.vcs-url="https://gitlab.com/mcf.io/docker-sabnzbd.git"

ENV HOME "/config"
ENV LANG C.UTF-8
ENV PYTHONIOENCODING UTF-8

WORKDIR /config

COPY --from=builder /build/par2cmdline/par2 /usr/bin
COPY --from=builder /build/SABnzbd /opt/SABnzbd
COPY --from=builder /build/mp4_automator /scripts/mp4_automator
COPY --from=builder /build/pip /usr

RUN apk add --no-cache \
    ca-certificates \
    ffmpeg \
    libgomp \
    p7zip \
    python \
    py-pip \
    unrar \
    unzip

COPY /docker-entrypoint.sh /

EXPOSE 8080 9090

ENTRYPOINT /docker-entrypoint.sh
