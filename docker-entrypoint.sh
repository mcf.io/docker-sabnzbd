#!/bin/sh

#
# Display settings on standard out.
#

USER="sabnzbd"

echo "SABnzbd settings"
echo "=================="
echo
echo "  User:       ${USER}"
echo "  UID:        ${PUID:=666}"
echo "  GID:        ${PGID:=666}"
echo
echo "  Config:     ${CONFIG:=/config}"
echo

#
# Change UID / GID of Sonarr user.
#

printf "Creating UID / GID... "
addgroup -S -g ${PGID} ${USER}
adduser -S -u ${PUID} -G ${USER} -h ${CONFIG} -s /bin/sh ${USER}
echo "[DONE]"

#
# Set directory permissions.
#

printf "Set permissions... "
chown -R ${USER}: /config
chown -R ${USER}: /scripts/mp4_automator
echo "[DONE]"

#
# Finally, start SABnzbd.
#

echo "Starting Sonarr..."
exec su -pc "python /opt/SABnzbd/SABnzbd.py --browser 0 --config-file ${CONFIG} --server 0.0.0.0" ${USER}
